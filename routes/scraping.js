const express = require('express');
const router = express.Router();
const analyzeWebs = require('../services/scraping')
const cors = require('cors');

const corsOptions = {
  origin: 'https://lideressociales.com',
  "methods": "GET,PUT,POST,DELETE",
  "preflightContinue": true,
  optionsSuccessStatus: 200
}

/* GET users listing. */
router.get('/news', cors(), analyzeWebs);
// router.put('/:id', cors(corsOptions), SCRAPING.updateUserPassword);

module.exports = router;
